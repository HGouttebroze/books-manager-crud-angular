import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  books: Array<any>;

  constructor() { 

    this.books = [];

    for(let i = 1; i <= 10; i++) {
      this.books.push(
        {
          id: i,
          title: 'Livre ' + i,
          author: 'Auteur ' + i,
          description: 'Description' + i,
          status: i % 2 === 0 ? 'Libre' : 'pris'
        }
      );
    }
  }

  getBookById(bookId: number): Promise<any> {
     const cb = (res, rej) => {
      for (const book of this.books) {
            if (book.id === bookId) {
              //setTimeout(())
             res(book);
            }
          }
      }
      return new Promise<any>(cb)
  }

  changeAllStatus(newStatus: string): Promise<any> {

    const cb = (res, rej) => {
      this.books.forEach(book => book.status = newStatus);
      res(this.books);
    };

    return new Promise<any>(cb);
    
  }

  changeStatus(bookId: number, newStatus: string): Promise<any> {

    return new Promise<any>((res, rej) => {
      for (const [index, book] of this.books.entries()) {
            if (book.id === bookId) {
              this.books[index].status = newStatus;
             res(this.books[index]);
            }
          }
      }
    );
    
  }
}
