import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BooksViewComponent } from './views/books-view/books-view.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import { DetailsBookViewComponent } from './views/details-book-view/details-book-view.component'

const routes: Routes = [
  {path: '', component: AuthViewComponent}, // can navigate anonymously
  {path: 'books', canActivate: [AuthGuard], component: BooksViewComponent},
  {path: 'book/:id', canActivate: [AuthGuard], component: DetailsBookViewComponent},
  {path: 'not-found', component: NotFoundViewComponent},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
