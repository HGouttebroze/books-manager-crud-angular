import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsBookViewComponent } from './details-book-view.component';

describe('DetailsBookViewComponent', () => {
  let component: DetailsBookViewComponent;
  let fixture: ComponentFixture<DetailsBookViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsBookViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsBookViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
