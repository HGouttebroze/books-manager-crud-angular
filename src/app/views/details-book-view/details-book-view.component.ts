import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {BookService} from '../../services/book/book.service';

@Component({
  selector: 'app-details-book-view',
  templateUrl: './details-book-view.component.html',
  styleUrls: ['./details-book-view.component.css']
})
export class DetailsBookViewComponent implements OnInit {

  book: any;
  constructor(private route: ActivatedRoute, private bookService: BookService) { }

  ngOnInit(): void {
    const id =this.route.snapshot.params.id;

    this.bookService.getBookById(+id)
    .then(book => this.book = book);

    console.log(id);
  }

}
