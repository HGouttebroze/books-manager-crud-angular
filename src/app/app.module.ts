import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BookService} from './services/book/book.service';
import { BooksViewComponent } from './views/books-view/books-view.component';
import { BooksTableComponent } from './components/books-table/books-table.component';
import { BookTableRowComponent } from './components/book-table-row/book-table-row.component';
import { AuthViewComponent } from './views/auth-view/auth-view.component';
import { DetailsBookViewComponent } from './views/details-book-view/details-book-view.component';
import { NotFoundViewComponent } from './views/not-found-view/not-found-view.component';
import {AuthService} from './services/auth/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    BooksViewComponent,
    BooksTableComponent,
    BookTableRowComponent,
    AuthViewComponent,
    DetailsBookViewComponent,
    NotFoundViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [AuthService, BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
