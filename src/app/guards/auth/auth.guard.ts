import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})

// implements CanActivate (ask when create guard)
export class AuthGuard implements CanActivate {
  
  token: boolean;

  constructor(private authService: AuthService, private router: Router){

  }
  canActivate(
    route: ActivatedRouteSnapshot,

    // attendu en sortie (on revoie ici 1 true  ou false)
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

  // if(!true.authService.token) {
  //     return this.router.navigateByUrl('');
  //   }
  //   return this.authService.token;
  // }
  if(this.authService) { 
      return true;
    }
    return false;
  }
}
